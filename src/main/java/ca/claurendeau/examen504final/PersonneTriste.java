package ca.claurendeau.examen504final;

public class PersonneTriste implements Personne{
    
    private String humeur;
    private String besoin;
    
    public PersonneTriste() {
        this.humeur = "triste";
        this.besoin = "Je fais parti des gens qui n'auront jamais de MacBook Pro";
    }

    @Override
    public String getHumeur() {
        return humeur;
    }

    @Override
    public String getBesoin() {
        return besoin;
    }
    
    @Override
    public void setBesoin(String besoin) {
        this.besoin = besoin;
    }
    
    @Override
    public String toString() {
        return "Personne [humeur=" + humeur + "]";
    }
    
}
