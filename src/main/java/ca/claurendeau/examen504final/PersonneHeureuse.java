package ca.claurendeau.examen504final;

public class PersonneHeureuse implements Personne{
    
    private String humeur;
    private String besoin;
    
    public PersonneHeureuse() {
        this.humeur = "heureuse";
        this.besoin = "J'ai un MacBook Pro, j'ai tout ce qu'il me faut  pour être une personne heureuse!";
    }

    @Override
    public String getHumeur() {
        return humeur;
    }

    @Override
    public String getBesoin() {
        return besoin;
    }
    
    @Override
    public void setBesoin(String besoin) {
        this.besoin = besoin;
    }
    
    @Override
    public String toString() {
        return "Personne [humeur=" + humeur + "]";
    }

}
