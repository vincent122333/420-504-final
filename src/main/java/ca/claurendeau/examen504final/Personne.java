package ca.claurendeau.examen504final;

/**
 * 
 * Remanier le code ci-dessous de façon à ce qu'il utilise le 'State/Strategy Pattern'
 * 
 * Vous devrez nommer chacun des remaniements que vous faites ainsi que de faire un commit à chaque remaniement.
 *
 */
public interface Personne {
    
    public String getHumeur();
    
    public String getBesoin();
    
    public void setBesoin(String besoin);
    
}
