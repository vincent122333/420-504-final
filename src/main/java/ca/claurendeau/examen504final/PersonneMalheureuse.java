package ca.claurendeau.examen504final;

public class PersonneMalheureuse implements Personne{
    
    private String humeur;
    private String besoin;
    
    public PersonneMalheureuse() {
        this.humeur = "malheureuse";
        this.besoin = "J'ai besoin d'un MacBook Pro pour être une personne heureuse!";
    }

    @Override
    public String getHumeur() {
        return humeur;
    }

    @Override
    public String getBesoin() {
        return besoin; 
    }
    
    @Override
    public void setBesoin(String besoin) {
       this.besoin = besoin;
    }
    
    @Override
    public String toString() {
        return "Personne [humeur=" + humeur + "]";
    }
}
