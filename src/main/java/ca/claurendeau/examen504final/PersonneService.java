package ca.claurendeau.examen504final;

import java.util.ArrayList;
import java.util.List;

public class PersonneService {
    
    public static void main(String[] args) {
        
        List<Personne> personnes = new ArrayList<>();
        personnes.add(new PersonneHeureuse());
        personnes.add(new PersonneMalheureuse());
        personnes.add(new PersonneTriste());
        
        personnes.stream()
                 .forEach(System.out::println);
        
        for (Personne personne : personnes) {
            afficheBesoin(personne);
        }
    }

    private static void afficheBesoin(Personne personne) {
        System.out.println(personne.getBesoin());
    }
}
