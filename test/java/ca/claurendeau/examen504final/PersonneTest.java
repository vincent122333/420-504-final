package ca.claurendeau.examen504final;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class PersonneTest {
    
    PersonneHeureuse personneHeureuse;
    PersonneMalheureuse personneMalheureuse;
    PersonneTriste personneTriste;
    
    @Before
    public void setUp() {
        personneHeureuse = new PersonneHeureuse();
        personneMalheureuse = new PersonneMalheureuse();
        personneTriste = new PersonneTriste();
    }
    
    @After
    public void tearDown() {
        personneHeureuse = null;
        personneMalheureuse = null;
        personneTriste = null;
    }
    
    /* Tests pour tester l'humeur des Personnes */

    @Test
    public void testGetHumeurPersonneHeureuse() {
        assertTrue(personneHeureuse.getHumeur().equals("heureuse"));
    }
    
    @Test
    public void testGetHumeurPersonneMalheureuse() {
        assertTrue(personneMalheureuse.getHumeur().equals("malheureuse"));
    }
    
    @Test
    public void testGetHumeurPersonneTriste() {
        assertTrue(personneTriste.getHumeur().equals("triste"));
    }
    
    /* Tests pour tester le besoin des Personnes */
    
    @Test
    public void testGetBesoinPersonneHeureuse() {
        assertTrue(personneHeureuse.getBesoin().equals("J'ai un MacBook Pro, j'ai tout ce qu'il me faut  pour être une personne heureuse!"));
    }
    
    @Test
    public void testGetBesoinPersonneMalheureuse() {
        assertTrue(personneMalheureuse.getBesoin().equals("J'ai besoin d'un MacBook Pro pour être une personne heureuse!"));
    }
    
    @Test
    public void testGetBesoinPersonneTriste() {
        assertTrue(personneTriste.getBesoin().equals("Je fais parti des gens qui n'auront jamais de MacBook Pro"));
    }
    
    /* Test pour tester la m�thode setBesoin pour chacune des classes qui impl�mentes Personne */
    
    @Test
    public void testSetBesoinPersonneHeureuse() {
        personneHeureuse.setBesoin("Allo");
        assertTrue(personneHeureuse.getBesoin().equals("Allo"));
    }
    
    @Test
    public void testSetBesoinPersonneMalheureuse() {
        personneMalheureuse.setBesoin("Allo");
        assertTrue(personneMalheureuse.getBesoin().equals("Allo"));
    }
    
    @Test
    public void testSetBesoinPersonneTriste() {
        personneTriste.setBesoin("Allo");
        assertTrue(personneTriste.getBesoin().equals("Allo"));
    }
}
